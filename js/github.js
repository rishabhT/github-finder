class Github {
  constructor() {
    this.clientid = "6da73ccebc9069bd9e81";
    this.clientsecret = "723cc572a2e5e5c59557ba9fb5e4d7474324e8da";
    this.repos_count = 5;
    this.repos_sort = "created: asc";
  }
  async getUser(user) {
    const profileResponse = await fetch(
      `https:api.github.com/users/${user}?client_id=${this.clientid}&client_secret=${this.clientsecret}`
    );
    const repoResponse = await fetch(
      `https:api.github.com/users/${user}/repos?per_page=${this.repos_count}&sort=${this.repos_sort}&client_id=${this.clientid}&client_secret=${this.clientsecret}`
    );
    const profile = await profileResponse.json();

    const repos = await repoResponse.json();
    return {
      profile,
      repos,
    };
  }
}
