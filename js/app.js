github = new Github();
ui = new UI();
const searchuser = document.getElementById("searchUser");

searchuser.addEventListener("keyup", (e) => {
  const userName = e.target.value;
  if (userName !== "") {
    github.getUser(userName).then((data) => {
      console.log(data);
      if (data.profile.message === "Not Found") {
        ui.showAlert("This username is not exists", "alert alert-danger");
      } else {
        ui.userProfile(data.profile);
        ui.userRepos(data.repos);
      }
    });
  } else {
    ui.clearProfile();
  }
});
