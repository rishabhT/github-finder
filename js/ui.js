class UI {
  constructor() {
    this.Profile = document.getElementById("profile");
  }

  userProfile(user) {
    this.Profile.innerHTML = `
    <div class="card card-body">
    <div class="row">
      <div class="col-md-3">
        <img class="img-fluid mb2" src="${user.avatar_url}" alt="" />
        <a
          href="${user.html_url}"
          class="btn btn-primary btn-block mt-1"
          target="_blanck"
          >View Profile</a
        >
      </div>
      <div class="col-md-9">
        <span class="badge badge-primary">Public Repos: ${user.public_repos}</span>
        <span class="badge badge-secondary">Public Gists: ${user.public_gists}</span>
        <span class="badge badge-success">Followers: ${user.followers}</span>
        <span class="badge badge-info">Followings: ${user.following}</span>

      <ul class="list-group mt-3">
        <li class="list-group-item">${user.company}</li>
        <li class="list-group-item">${user.blog}</li>
        <li class="list-group-item">${user.location}</li>
        <li class="list-group-item">${user.created_at}</li>
      </ul>
      </div>
    </div>
    <h3>Letest Repos</h3>
    <div id = "repos"></div>
  </div>
        `;
  }

  clearProfile() {
    this.Profile.innerHTML = "";
  }

  userRepos(repos) {
    let output = "";
    repos.forEach((repo) => {
      output += `
        <div class="card card-body mb-2">
            <div class="row">
                <div class="col-md-6">
                    <a href="${repo.html_url}" target="_blank">${repo.name}</a>
                </div>
                <div class="col-md-6">
                    <span class="badge badge-primary">Stars: ${repo.stargazers_count}</span>
                    <span class="badge badge-secondary">Watchers: ${repo.watchers}</span>
                    <span class="badge badge-success">Forks: ${repo.forms_count}</span>
                </div>
            </div>
        </div>
        `;
    });
    document.getElementById("repos").innerHTML = output;
  }

  showAlert(message, classname) {
    this.clearAlert();

    const div = document.createElement("div");
    div.className = classname;
    div.appendChild(document.createTextNode(message));
    const container = document.querySelector(".searchbox");
    const search = document.querySelector(".card");
    container.insertBefore(div, search);

    setTimeout(() => {
      this.clearAlert();
    }, 3000);
  }

  clearAlert() {
    const alert = document.querySelector(".alert");
    if (alert) {
      alert.remove();
    }
  }
}
